﻿/*Task 9:Animal Park
 
 * Create an Animal class
 • Provide properties
 • Provide behaviours
 • Provide an overloaded constructor
 • Create multiple Animal objects and assign the appropriate properties to represent at least three different animals
 • Store them in a Collection of your choice
 */
using System;

namespace AnimalPark
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal Dog = new Animal("Rex",3,"Dog");
            Animal Bird = new Animal("Titi", 4, "Bird");
            Animal Fish = new Animal("Fifi", 10, "Fish");
            Animal Snake = new Animal("Nishnish", 6, "Cat");
            //Console.WriteLine(Cat.Age = 9);
            Dog.AnimalPrint();
            Dog.AnimalSound();
            Dog.AnimalMove();

            Bird.AnimalPrint();
            Bird.AnimalSound();
            Bird.AnimalMove();

        }
    }
}
