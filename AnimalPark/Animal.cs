﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalPark
{
    class Animal
    {
        private String name;
        private int age;
        private String species;
        private String sound;

        public Animal()
        {

        }
        public Animal(String name,int age,String species)
        {
            this.name = name;
            this.age=age;
            this.species = species;
            
        }

        public string Name { get => name; set => name = value; }
        public int Age { get => age; set => age = value; }
        public string Species { get => species; set => species = value; }
        public string Sound { get => sound; set => sound = value; }

        // Create Methoes
        public void AnimalPrint()
        {
            Console.WriteLine("name is "+name+" with the age "+age+ " and it is "+species);
        }

        public void AnimalSound()
        {
            if (species=="Dog")
            {
                Console.WriteLine("Dogs bark");
            }
            else if (species=="Bird")
            {
                Console.WriteLine("Bird sound is ChikChik");
            }
            else if (species == "Fish")
            {
                Console.WriteLine("Fishes does not have sound ");
            }
            else if (species == "Snake")
            {
                Console.WriteLine("Snake sound is Shshshshshs");
            }



        }
        public void AnimalMove()
        {
            if(species=="Dog")
            {
                Console.WriteLine(species+" are running");
            }
            else if (species == "Bird")
            {
                Console.WriteLine(species + " are flying");
            }
            else if (species == "Fish")
            {
                Console.WriteLine(species + " are swimming");
            }
            else if (species == "Snake")
            {
                Console.WriteLine(species + " are crawling");
            }
        }

    }
   
}
